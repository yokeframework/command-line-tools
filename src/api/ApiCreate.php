<?php
declare (strict_types = 1);

namespace command\api;

use command\api\YapiService;
use command\lib\DocParse;
use ReflectionClass;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class ApiCreate extends Command
{

    protected function configure()
    {
        // 指令配置
        $this->setName('ykapi:create')
            ->addArgument('module', Argument::OPTIONAL, "app module name")
            ->setDescription('auto update model');        
    }

    protected function execute(Input $input, Output $output)
    {
        $module = $input->getArgument('module');
        $modulePath = $module?$module."/":'';

        $path = $this->app->getBasePath().$modulePath."controller";

        if (!is_dir($path)) {
            $output->writeln('path is not exist');
            return;
        }
        
        $list = scandir($path);//扫描文件夹，获取文件列表

        $namespace = 'app\controller';

        if($module)$namespace = 'app\\'.$module.'\controller';

        $routeStr = '';

        $yapi = new YapiService($module);

        $controller = config('ykcommand.auto.'.($module?$module.'.controller':'controller'));//如果指定控制器自动生成

        foreach ($list as $key => $value) {
            
            if(is_file($path.'/'.$value)){
                $arr = explode(".",$value);
                if(!empty($controller)&&!in_array($arr[0],$controller))continue;
                
                $class = new ReflectionClass($namespace.'\\'.$arr[0]);//反射类，获取类的对象

                $classDoc = new DocParse($class->getDocComment()??'');//获取并解析类注释

                $cate_name = $classDoc->getApiCat()?:($classDoc->getComment());
                $tokenExcept = $classDoc->getTokenExcept();

                $methods = $class->getMethods();//获取类方法列表
                foreach ($methods as $method) {
                    
                    if($class->getName()!=$method->getDeclaringClass()->getName())continue;//如果是继承的方法跳过
                    $doc = $method->getDocComment();//获取方法注释
                    $methodDoc = new DocParse($doc,$class->getShortName().'/'.$method->getName());//解析方法注释

                    $api=[];//遍历开始时清空api数组
                    $api['cate_name'] = $methodDoc->getApiCat()?:($cate_name?:'公共分类');

                    if(in_array($method->getName(),$tokenExcept)){
                        $api['except_token']=1;
                    }

                    $api['title']=$methodDoc->getComment();
                    $api['path']=$methodDoc->getPath();
                    $api['http_method']=$methodDoc->getHttpMethod();

                    $reqarr = $methodDoc->getReq();

                    $req=[];
                    foreach ($reqarr as $value) {
                        $tempArr = explode('|',$value);
                        $value = [
                            'name'=>$tempArr[0],
                            'required'=>$tempArr[1]??1,
                            'desc'=>$tempArr[2]??'',
                            'type'=>$tempArr[3]??'text'
                        ];
                        $req[]=$value;
                    }

                    $api['req']=$req;

                    $yapi->syncApi($api);
                }

            }

        }

    	// 指令输出
    	$output->writeln('api create success');
    }

    
}
