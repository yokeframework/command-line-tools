<?php
namespace command\factory;

use command\factory\config\ApiParse;
use command\factory\config\ModelParse;
use command\factory\config\ServiceParse;

/**
 * 构建自动化命令配置
 */
class ConfigParse
{
    private static $config=[
        'db' => [],
        'model' => [],
        'service' => [],
        'api' => [],
    ];
    
    /**
     * 构建配置
     *
     * @param array $config
     * @return void
     */
    public static function parse($config)
    {
        $servicePool = $config['service'];

        $modelArr = [];
        $serviceArr = [];

        foreach ($servicePool as $name => $service) {
            $modelArr = array_merge($modelArr,$service['model']);
            $serviceArr[$name]=$service['method'];
        }

        $apiPool = $config['api'];

        ModelParse::parse($modelArr,static::$config);
        ServiceParse::parse($serviceArr,static::$config);
        ApiParse::parse($apiPool,static::$config);

        return static::$config;
    }
    
}