<?php
namespace command\factory\config;

class ModelParse
{
    /**
     * 构建模型配置
     *
     * @param array $modelArr
     * @return void
     */
    public static function parse($modelArr,&$config)
    {
        $static = new static();

        $db = [];
        $model = [];
        
        foreach ($modelArr as $table => $set) {
            $table = explode('|',$table);
            $fieldConf = $static->buildField($set['field'],$set['pk']);
            $db[$table[0]]=[
                'table'=>$table[0],
                'pk'=>$set['pk'],
                'field'=>$fieldConf['field_sql'],
                'engine'=>$set['engine']
            ];

            $model[$table[0]]=[
                'namespace'=>'app\model',
                'name'=>$table[0],
                'comment'=>$table[1]??$table[0],
                'pk'=>$set['pk'],
                'schema'=>$fieldConf['schema'],
                'relation'=>$static->buildRelation($set['relation']??''),
                'allow_field'=>$fieldConf['allow_field'],
                'query'=>$fieldConf['query'],
                'require'=>$fieldConf['require'],
                'field'=>$fieldConf['m_field']
            ];
        }

        $config['db']=$db;
        $config['model']=$model;

        return true;
    }

    /**
     * 构建字段配置
     *
     * @param string $fieldConfigStr
     * @return void
     */
    private function buildField($fieldConfigStr,$pk)
    {
        $fieldArr = explode(',',$fieldConfigStr);

        $schema = '['.PHP_EOL;//模型模式配置
        $allowField = [];//可编辑字段
        $query = [];//可查询字段
        $require = [];//必填字段
        $resArr = [];//create sql 数组
        $mField = [];
        foreach ($fieldArr as $field) {
            $fieldInfo = explode('|',$field);

            $extInfo = [];
            foreach ($fieldInfo as $key => $value) {
                if($key<3)continue;
                list($k,$v)=explode(':',$value);
                $extInfo[$k]=$v;
            }
            $temp = [];
            $temp[]='`'.$fieldInfo[0].'`';
            $temp[]=str_replace('#',',',$fieldInfo[1]);
            $temp[]=$extInfo['n']??'DEFAULT';
            $temp[]=$extInfo['d']??'NULL';
            if($fieldInfo[0]==$pk)$temp[]='AUTO_INCREMENT';
            if(!empty($fieldInfo[2]))$temp[]='COMMENT "'.$fieldInfo[2].'"';

            $mField[$fieldInfo[0]]=$fieldInfo[2]??'';
            
            $resArr[]=$temp;
            //模型模式配置
            $schema.='        "'.$fieldInfo[0].'"=>"'.str_replace('#',',',$fieldInfo[1]).'",'.PHP_EOL;
            //可编辑字段
            if(!empty($extInfo['u'])){
                $allowField['field'][] = $fieldInfo[0];
                $allowField['param'][] = $fieldInfo[0].'|0|'.($fieldInfo[2]??'');
            }
            //可查询字段
            if(!empty($extInfo['q'])){
                $query['wconfig'][$fieldInfo[0]] = $this->buildQuery($fieldInfo[0],$extInfo['q']);
                $query['param'][] = $fieldInfo[0].'|0|'.($fieldInfo[2]??'');
            }

            if($fieldInfo[0]!=$pk){
                if(empty($extInfo['b'])){
                    $require[]=$fieldInfo[0].'|0|'.($fieldInfo[2]??'');
                }else{
                    $require[]=$fieldInfo[0].'|1|'.($fieldInfo[2]??'');
                }
            }
            
        }

        $schema = trim($schema,',')."    ]";

        return ['field_sql'=>$resArr,'schema'=>$schema,'allow_field'=>$allowField,'query'=>$query,'require'=>$require,'m_field'=>$mField];
    }

    /**
     * 解析关联关系配置
     *
     * @param string $relations
     * @return void
     */
    private function buildRelation($relations)
    {
        if(empty($relations))return '';
        $relations = explode(',',$relations);

        $functions = [];
        foreach ($relations as $relation) {
            $temp = explode('|',$relation);
            $tempinfo = [];
            $tempinfo['relation_name']=$temp[0];
            $tempinfo['comment']=$temp[1];
            $tempinfo['model']=$temp[2];
            $tempinfo['type']=ucfirst($temp[3]);
            $tempinfo['bind']=$temp[4]??'';
            $functions[$temp[0]]=$tempinfo;
        }

        return $functions;
    }

    /**
     * 构建查询条件
     *
     * @param string $field
     * @param string $config
     * @return void
     */
    public function buildQuery($field,$config)
    {
        $config = explode('&',$config);

        $flag = $config[1]??'';

        switch ($flag) {
            case 'l':
                $config = [$field,'like'];
                break;
            case 't':
                $config = [$field,'time'];
                break;
            case 'in':
                $config = [$field,'in'];
            case 'neq':
                $config = [$field,'<>'];
            case 'gt':
                $config = [$field,'>'];
            default:
                $config = [$field,'='];
                break;
        }

        return $config;
    }


}