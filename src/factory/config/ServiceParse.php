<?php
namespace command\factory\config;

class ServiceParse
{
    /**
     * 构建服务配置
     *
     * @param array $serviceArr
     * @return void
     */
    public static function parse($serviceArr,&$config)
    {
        $static = new static();

        $static->config = $config;

        $serviceList = [];
        foreach ($serviceArr as $name => $methods) {
            $name = explode('|',$name);
            
            $methodConf = $static->buildServiceMethod($methods);
            $serviceList[$name[0]] = [
                'namespace'=>'app\service',
                'comment'=>$name[1]??'',
                'service_name'=>ucfirst($name[0]).'Service',
                'methods'=>$methodConf['functions'],
                'use'=>$methodConf['use']
            ];
        }

        $config['service']=$serviceList;

        return true;
    }

     /**
     * 解析服务方法配置
     *
     * @param array $methods
     * @return void
     */
    private function buildServiceMethod($methods)
    {
        $methodArr = [];
        $this->use = [];
        foreach ($methods as $name => $config) {
            $name = explode('|',$name);
            
            if(is_array($config)){
                foreach ($config as $subconf) {
                    $itemInfo = $this->buildServiceMethodItem($subconf,$name,1);
                    $methodArr[$itemInfo['method_name']]=$itemInfo;
                }
            }else{
                $itemInfo = $this->buildServiceMethodItem($config,$name);
                $methodArr[$itemInfo['method_name']]=$itemInfo;
            }            
        }

        return ['use'=>$this->use,'functions'=>$methodArr];
    }

    /**
     * 解析服务方法配置细项
     *
     * @param string $itemConfig
     * @param string $pk
     * @param string $comment
     * @return void
     */
    private function buildServiceMethodItem($itemConfig,$name,$sub=0)
    {
        $itemArr = explode('|',$itemConfig);

        $model = $name[2]??ucfirst($name[0]);
        $use='use app\\model\\'.$model.';';
        if(!in_array($use,$this->use))$this->use[]=$use;
        $comment = $name[1];
        $modelInfo = $this->config['model'][$model];
        $pk = $modelInfo['pk'];

        $extInfo = [];
        foreach ($itemArr as $key => $value) {
            if($key<1)continue;
            list($k,$v)=explode(':',$value);
            $extInfo[$k]=$v;
        }

        $type = $itemArr[0];

        switch ($type) {
            case 'add':
                $comment = '新增'.$comment;
                break;
            case 'update':
                $comment = '更新'.$comment;
                break;
            case 'list':
                $with = $extInfo['with']??'[]';
                $query = $extInfo['q']??[];
                $order = $extInfo['sort']??'"'.$pk.' DESC"';
                $comment = '查询'.$comment.'列表';
                break;
            case 'delete':
                $comment = '删除'.$comment;
                break;
            case 'find':
                $with = $extInfo['with']??'[]';
                $query = $extInfo['q']??[];
                $comment = '查询'.$comment.'详情';
                break;
            case 'info':
                $with = $extInfo['with']??'[]';
                $comment = '获取'.$comment.'详情';
                break;
            case 'batchAdd':
                $comment = '新增'.$comment.'（批量）';
                break;
            case 'batchUpdate':
                $comment = '更新'.$comment.'（批量）';
                break;
            case 'batchDelete':
                $comment = '删除'.$comment.'（批量）';
                break;    
            default:
                return_error(['info'=>'服务方法类型参数异常']);
                break;
        }

        return  [
                    'method_name'=>$sub?$name[0].ucfirst($type):$name[0],
                    'alias_name'=>$name[0],
                    'comment'=>$sub?$comment:$name[1],
                    'model'=>$model,
                    'type'=>$type,
                    'pk'=>$pk,
                    'allow_field'=>$modelInfo['allow_field'],
                    'with'=>$with??'[]',
                    'order'=>$order??'',
                    'query'=>empty($query)?$modelInfo['query']:$this->buildQuery($query,$modelInfo),
                    'require'=>$modelInfo['require']
                ];

    }

    /**
     * 构建查询条件
     *
     * @param string $qstr
     * @param array $modelInfo
     * @return void
     */
    private function buildQuery($qstr,$modelInfo)
    {
        $arr = explode(',',$qstr);
        $query = [];
        foreach ($arr as $value) {
            $fieldInfo = explode('&',$value);
            $flag = $fieldInfo[1]??'';
            $field = $fieldInfo[0];
            switch ($flag) {
                case 'l':
                    $config = [$field,'like'];
                    break;
                case 't':
                    $config = [$field,'time'];
                    break;
                case 'in':
                    $config = [$field,'in'];
                case 'neq':
                    $config = [$field,'<>'];
                case 'gt':
                    $config = [$field,'>'];
                default:
                    $config = [$field,'='];
                    break;
            }
            $query['wconfig'][$field] = $config;
            $query['param'][] = $field.'|0|'.($modelInfo['field'][$field]??'');
        }
        

        return $query;
    }


}