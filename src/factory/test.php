array (
  'db' => 
  array (
    'Test' => 
    array (
      'table' => 'Test',
      'pk' => 'test_id',
      'field' => 
      array (
        0 => 
        array (
          0 => 'test_id',
          1 => 'int(11)',
          2 => 'DEFAULT',
          3 => 'AUTO_INCREMENT',
        ),
        1 => 
        array (
          0 => 'test_name',
          1 => 'varchar(100)',
          2 => 'DEFAULT',
          3 => 'COMMENT 名称',
        ),
        2 => 
        array (
          0 => 'test_desc',
          1 => 'text',
          2 => 'DEFAULT',
          3 => 'COMMENT 描述',
        ),
        3 => 
        array (
          0 => 'price',
          1 => 'decimal(10,2)',
          2 => 'DEFAULT',
          3 => '0.00',
          4 => 'COMMENT 价格',
        ),
        4 => 
        array (
          0 => 'status',
          1 => 'tinyint(4)',
          2 => 'DEFAULT',
          3 => 'COMMENT 状态&1 启用 0 禁用',
        ),
      ),
      'engine' => 'InnoDB',
    ),
    'TestChild' => 
    array (
      'table' => 'TestChild',
      'pk' => 'test_child_id',
      'field' => 
      array (
        0 => 
        array (
          0 => 'test_child_id',
          1 => 'int(11)',
          2 => 'DEFAULT',
          3 => 'AUTO_INCREMENT',
        ),
        1 => 
        array (
          0 => 'child_name',
          1 => 'varchar(100)',
          2 => 'DEFAULT',
          3 => 'COMMENT 子名称',
        ),
        2 => 
        array (
          0 => 'test_id',
          1 => 'int(11)',
          2 => 'DEFAULT',
          3 => 'COMMENT 测试ID',
        ),
      ),
      'engine' => 'InnoDB',
    ),
  ),
  'model' => 
  array (
    'Test' => 
    array (
      'namespace' => 'app\\model',
      'name' => 'Test',
      'comment' => '测试',
      'pk' => 'test_id',
      'schema' => '[
        "test_id"=>"int(11)",
        "test_name"=>"varchar(100)",
        "test_desc"=>"text",
        "price"=>"decimal(10,2)",
        "status"=>"tinyint(4)",
    ]',
      'relation' => 
      array (
        'child' => 
        array (
          'relation_name' => 'child',
          'comment' => '子数据',
          'model' => 'TestChild',
          'type' => 'HasMany',
          'bind' => '',
        ),
      ),
      'allow_field' => 
      array (
        'field' => 
        array (
          0 => 'test_name',
          1 => 'test_desc',
          2 => 'price',
          3 => 'status',
        ),
        'param' => 
        array (
          0 => 'test_name|0|名称',
          1 => 'test_desc|0|描述',
          2 => 'price|0|价格',
          3 => 'status|0|状态&1 启用 0 禁用',
        ),
      ),
      'query' => 
      array (
        'wconfig' => 
        array (
          'test_name' => 
          array (
            0 => 'test_name',
            1 => 'like',
          ),
          'price' => 
          array (
            0 => 'price',
            1 => '=',
          ),
          'status' => 
          array (
            0 => 'status',
            1 => '=',
          ),
        ),
        'param' => 
        array (
          0 => 'test_name|0|名称',
          1 => 'price|0|价格',
          2 => 'status|0|状态&1 启用 0 禁用',
        ),
      ),
      'require' => 
      array (
        0 => 'test_name|1|名称',
        1 => 'test_desc|0|描述',
        2 => 'price|1|价格',
        3 => 'status|0|状态&1 启用 0 禁用',
      ),
      'field' => 
      array (
        'test_id' => '',
        'test_name' => '名称',
        'test_desc' => '描述',
        'price' => '价格',
        'status' => '状态&1 启用 0 禁用',
      ),
    ),
    'TestChild' => 
    array (
      'namespace' => 'app\\model',
      'name' => 'TestChild',
      'comment' => '测试子数据',
      'pk' => 'test_child_id',
      'schema' => '[
        "test_child_id"=>"int(11)",
        "child_name"=>"varchar(100)",
        "test_id"=>"int(11)",
    ]',
      'relation' => 
      array (
        'test' => 
        array (
          'relation_name' => 'test',
          'comment' => '父数据',
          'model' => 'Test',
          'type' => 'BelongsTo',
          'bind' => 'test_name',
        ),
      ),
      'allow_field' => 
      array (
        'field' => 
        array (
          0 => 'child_name',
        ),
        'param' => 
        array (
          0 => 'child_name|0|子名称',
        ),
      ),
      'query' => 
      array (
        'wconfig' => 
        array (
          'child_name' => 
          array (
            0 => 'child_name',
            1 => 'like',
          ),
        ),
        'param' => 
        array (
          0 => 'child_name|0|子名称',
        ),
      ),
      'require' => 
      array (
        0 => 'child_name|1|子名称',
        1 => 'test_id|1|测试ID',
      ),
      'field' => 
      array (
        'test_child_id' => '',
        'child_name' => '子名称',
        'test_id' => '测试ID',
      ),
    ),
  ),
  'service' => 
  array (
    'test' => 
    array (
      'namespace' => 'app\\service',
      'comment' => '测试服务',
      'service_name' => 'TestService',
      'methods' => 
      array (
        'testAdd' => 
        array (
          'method_name' => 'testAdd',
          'alias_name' => 'test',
          'comment' => '新增测试数据',
          'model' => 'Test',
          'type' => 'add',
          'pk' => 'test_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'test_name',
              1 => 'test_desc',
              2 => 'price',
              3 => 'status',
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'test_desc|0|描述',
              2 => 'price|0|价格',
              3 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'test_name' => 
              array (
                0 => 'test_name',
                1 => 'like',
              ),
              'price' => 
              array (
                0 => 'price',
                1 => '=',
              ),
              'status' => 
              array (
                0 => 'status',
                1 => '=',
              ),
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'price|0|价格',
              2 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'require' => 
          array (
            0 => 'test_name|1|名称',
            1 => 'test_desc|0|描述',
            2 => 'price|1|价格',
            3 => 'status|0|状态&1 启用 0 禁用',
          ),
        ),
        'testUpdate' => 
        array (
          'method_name' => 'testUpdate',
          'alias_name' => 'test',
          'comment' => '更新测试数据',
          'model' => 'Test',
          'type' => 'update',
          'pk' => 'test_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'test_name',
              1 => 'test_desc',
              2 => 'price',
              3 => 'status',
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'test_desc|0|描述',
              2 => 'price|0|价格',
              3 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'test_name' => 
              array (
                0 => 'test_name',
                1 => 'like',
              ),
              'price' => 
              array (
                0 => 'price',
                1 => '=',
              ),
              'status' => 
              array (
                0 => 'status',
                1 => '=',
              ),
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'price|0|价格',
              2 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'require' => 
          array (
            0 => 'test_name|1|名称',
            1 => 'test_desc|0|描述',
            2 => 'price|1|价格',
            3 => 'status|0|状态&1 启用 0 禁用',
          ),
        ),
        'testList' => 
        array (
          'method_name' => 'testList',
          'alias_name' => 'test',
          'comment' => '查询测试数据列表',
          'model' => 'Test',
          'type' => 'list',
          'pk' => 'test_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'test_name',
              1 => 'test_desc',
              2 => 'price',
              3 => 'status',
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'test_desc|0|描述',
              2 => 'price|0|价格',
              3 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => 'test_id DESC',
          'query' => 
          array (
            'wconfig' => 
            array (
              'test_name' => 
              array (
                0 => 'test_name',
                1 => 'like',
              ),
              'price' => 
              array (
                0 => 'price',
                1 => '=',
              ),
              'status' => 
              array (
                0 => 'status',
                1 => '=',
              ),
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'price|0|价格',
              2 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'require' => 
          array (
            0 => 'test_name|1|名称',
            1 => 'test_desc|0|描述',
            2 => 'price|1|价格',
            3 => 'status|0|状态&1 启用 0 禁用',
          ),
        ),
        'testDelete' => 
        array (
          'method_name' => 'testDelete',
          'alias_name' => 'test',
          'comment' => '删除测试数据',
          'model' => 'Test',
          'type' => 'delete',
          'pk' => 'test_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'test_name',
              1 => 'test_desc',
              2 => 'price',
              3 => 'status',
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'test_desc|0|描述',
              2 => 'price|0|价格',
              3 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'test_name' => 
              array (
                0 => 'test_name',
                1 => 'like',
              ),
              'price' => 
              array (
                0 => 'price',
                1 => '=',
              ),
              'status' => 
              array (
                0 => 'status',
                1 => '=',
              ),
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'price|0|价格',
              2 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'require' => 
          array (
            0 => 'test_name|1|名称',
            1 => 'test_desc|0|描述',
            2 => 'price|1|价格',
            3 => 'status|0|状态&1 启用 0 禁用',
          ),
        ),
        'testInfo' => 
        array (
          'method_name' => 'testInfo',
          'alias_name' => 'test',
          'comment' => '获取测试数据详情',
          'model' => 'Test',
          'type' => 'info',
          'pk' => 'test_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'test_name',
              1 => 'test_desc',
              2 => 'price',
              3 => 'status',
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'test_desc|0|描述',
              2 => 'price|0|价格',
              3 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'test_name' => 
              array (
                0 => 'test_name',
                1 => 'like',
              ),
              'price' => 
              array (
                0 => 'price',
                1 => '=',
              ),
              'status' => 
              array (
                0 => 'status',
                1 => '=',
              ),
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'price|0|价格',
              2 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'require' => 
          array (
            0 => 'test_name|1|名称',
            1 => 'test_desc|0|描述',
            2 => 'price|1|价格',
            3 => 'status|0|状态&1 启用 0 禁用',
          ),
        ),
        'testFind' => 
        array (
          'method_name' => 'testFind',
          'alias_name' => 'test',
          'comment' => '查询测试数据详情',
          'model' => 'Test',
          'type' => 'find',
          'pk' => 'test_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'test_name',
              1 => 'test_desc',
              2 => 'price',
              3 => 'status',
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'test_desc|0|描述',
              2 => 'price|0|价格',
              3 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'test_name' => 
              array (
                0 => 'test_name',
                1 => 'like',
              ),
              'price' => 
              array (
                0 => 'price',
                1 => '=',
              ),
              'status' => 
              array (
                0 => 'status',
                1 => '=',
              ),
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'price|0|价格',
              2 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'require' => 
          array (
            0 => 'test_name|1|名称',
            1 => 'test_desc|0|描述',
            2 => 'price|1|价格',
            3 => 'status|0|状态&1 启用 0 禁用',
          ),
        ),
        'childAdd' => 
        array (
          'method_name' => 'childAdd',
          'alias_name' => 'child',
          'comment' => '新增测试子数据',
          'model' => 'TestChild',
          'type' => 'add',
          'pk' => 'test_child_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'child_name',
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'child_name' => 
              array (
                0 => 'child_name',
                1 => 'like',
              ),
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'require' => 
          array (
            0 => 'child_name|1|子名称',
            1 => 'test_id|1|测试ID',
          ),
        ),
        'childUpdate' => 
        array (
          'method_name' => 'childUpdate',
          'alias_name' => 'child',
          'comment' => '更新测试子数据',
          'model' => 'TestChild',
          'type' => 'update',
          'pk' => 'test_child_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'child_name',
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'child_name' => 
              array (
                0 => 'child_name',
                1 => 'like',
              ),
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'require' => 
          array (
            0 => 'child_name|1|子名称',
            1 => 'test_id|1|测试ID',
          ),
        ),
        'childList' => 
        array (
          'method_name' => 'childList',
          'alias_name' => 'child',
          'comment' => '查询测试子数据列表',
          'model' => 'TestChild',
          'type' => 'list',
          'pk' => 'test_child_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'child_name',
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'with' => 'test',
          'order' => 'test_child_id DESC',
          'query' => 
          array (
            'wconfig' => 
            array (
              'child_name' => 
              array (
                0 => 'child_name',
                1 => 'like',
              ),
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'require' => 
          array (
            0 => 'child_name|1|子名称',
            1 => 'test_id|1|测试ID',
          ),
        ),
        'childDelete' => 
        array (
          'method_name' => 'childDelete',
          'alias_name' => 'child',
          'comment' => '删除测试子数据',
          'model' => 'TestChild',
          'type' => 'delete',
          'pk' => 'test_child_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'child_name',
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'child_name' => 
              array (
                0 => 'child_name',
                1 => 'like',
              ),
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'require' => 
          array (
            0 => 'child_name|1|子名称',
            1 => 'test_id|1|测试ID',
          ),
        ),
        'childInfo' => 
        array (
          'method_name' => 'childInfo',
          'alias_name' => 'child',
          'comment' => '获取测试子数据详情',
          'model' => 'TestChild',
          'type' => 'info',
          'pk' => 'test_child_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'child_name',
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'child_name' => 
              array (
                0 => 'child_name',
                1 => 'like',
              ),
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'require' => 
          array (
            0 => 'child_name|1|子名称',
            1 => 'test_id|1|测试ID',
          ),
        ),
        'childBatchAdd' => 
        array (
          'method_name' => 'childBatchAdd',
          'alias_name' => 'child',
          'comment' => '新增测试子数据（批量）',
          'model' => 'TestChild',
          'type' => 'batchAdd',
          'pk' => 'test_child_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'child_name',
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'child_name' => 
              array (
                0 => 'child_name',
                1 => 'like',
              ),
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'require' => 
          array (
            0 => 'child_name|1|子名称',
            1 => 'test_id|1|测试ID',
          ),
        ),
        'childBatchUpdate' => 
        array (
          'method_name' => 'childBatchUpdate',
          'alias_name' => 'child',
          'comment' => '更新测试子数据（批量）',
          'model' => 'TestChild',
          'type' => 'batchUpdate',
          'pk' => 'test_child_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'child_name',
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'child_name' => 
              array (
                0 => 'child_name',
                1 => 'like',
              ),
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'require' => 
          array (
            0 => 'child_name|1|子名称',
            1 => 'test_id|1|测试ID',
          ),
        ),
        'childBatchDelete' => 
        array (
          'method_name' => 'childBatchDelete',
          'alias_name' => 'child',
          'comment' => '删除测试子数据（批量）',
          'model' => 'TestChild',
          'type' => 'batchDelete',
          'pk' => 'test_child_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'child_name',
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'child_name' => 
              array (
                0 => 'child_name',
                1 => 'like',
              ),
            ),
            'param' => 
            array (
              0 => 'child_name|0|子名称',
            ),
          ),
          'require' => 
          array (
            0 => 'child_name|1|子名称',
            1 => 'test_id|1|测试ID',
          ),
        ),
        'testCheck' => 
        array (
          'method_name' => 'testCheck',
          'alias_name' => 'testCheck',
          'comment' => '检测测试数据',
          'model' => 'Test',
          'type' => 'find',
          'pk' => 'test_id',
          'allow_field' => 
          array (
            'field' => 
            array (
              0 => 'test_name',
              1 => 'test_desc',
              2 => 'price',
              3 => 'status',
            ),
            'param' => 
            array (
              0 => 'test_name|0|名称',
              1 => 'test_desc|0|描述',
              2 => 'price|0|价格',
              3 => 'status|0|状态&1 启用 0 禁用',
            ),
          ),
          'with' => 
          array (
          ),
          'order' => '',
          'query' => 
          array (
            'wconfig' => 
            array (
              'test_id' => 
              array (
                0 => 'test_id',
                1 => '=',
              ),
            ),
            'param' => 
            array (
              0 => 'test_id|0|',
            ),
          ),
          'require' => 
          array (
            0 => 'test_name|1|名称',
            1 => 'test_desc|0|描述',
            2 => 'price|1|价格',
            3 => 'status|0|状态&1 启用 0 禁用',
          ),
        ),
      ),
      'use' => 
      array (
        0 => 'use app\\model\\Test;',
        1 => 'use app\\model\\TestChild;',
      ),
    ),
  ),
  'api' => 
  array (
    'admin' => 
    array (
      'controller' => 
      array (
        'test' => 
        array (
          'comment' => '测试数据',
          'controller_name' => 'test',
          'api_cat' => '测试数据',
          'except_token' => 'testcheck',
          'methods' => 
          array (
            0 => 
            array (
              'method_name' => 'testAdd',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'testAdd',
              'comment' => '新增测试数据',
              'controller_name' => 'test',
              'call_pk' => 'test_id',
              'call_type' => 'add',
              'route' => '/test/add',
              'req_method' => 'post',
              'param' => 
              array (
                0 => 'test_name|1|名称',
                1 => 'test_desc|0|描述',
                2 => 'price|1|价格',
                3 => 'status|0|状态&1 启用 0 禁用',
              ),
            ),
            1 => 
            array (
              'method_name' => 'testUpdate',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'testUpdate',
              'comment' => '更新测试数据',
              'controller_name' => 'test',
              'call_pk' => 'test_id',
              'call_type' => 'update',
              'route' => '/test/update',
              'req_method' => 'post',
              'param' => 
              array (
                0 => 'test_id|1|',
                1 => 'test_name|0|名称',
                2 => 'test_desc|0|描述',
                3 => 'price|0|价格',
                4 => 'status|0|状态&1 启用 0 禁用',
              ),
            ),
            2 => 
            array (
              'method_name' => 'testList',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'testList',
              'comment' => '查询测试数据列表',
              'controller_name' => 'test',
              'call_pk' => 'test_id',
              'call_type' => 'list',
              'route' => '/test/list',
              'req_method' => 'get',
              'param' => 
              array (
                0 => 'test_name|0|名称',
                1 => 'price|0|价格',
                2 => 'status|0|状态&1 启用 0 禁用',
              ),
            ),
            3 => 
            array (
              'method_name' => 'testDelete',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'testDelete',
              'comment' => '删除测试数据',
              'controller_name' => 'test',
              'call_pk' => 'test_id',
              'call_type' => 'delete',
              'route' => '/test/delete',
              'req_method' => 'post',
              'param' => 
              array (
                0 => 'test_id|1|',
              ),
            ),
            4 => 
            array (
              'method_name' => 'testInfo',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'testInfo',
              'comment' => '获取测试数据详情',
              'controller_name' => 'test',
              'call_pk' => 'test_id',
              'call_type' => 'info',
              'route' => '/test/info',
              'req_method' => 'get',
              'param' => 
              array (
                0 => 'test_id|1|',
              ),
            ),
            5 => 
            array (
              'method_name' => 'testFind',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'testFind',
              'comment' => '查询测试数据详情',
              'controller_name' => 'test',
              'call_pk' => 'test_id',
              'call_type' => 'find',
              'route' => '/test/find',
              'req_method' => 'get',
              'param' => 
              array (
                0 => 'test_name|0|名称',
                1 => 'price|0|价格',
                2 => 'status|0|状态&1 启用 0 禁用',
              ),
            ),
            6 => 
            array (
              'method_name' => 'childAdd',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'childAdd',
              'comment' => '新增测试子数据',
              'controller_name' => 'test',
              'call_pk' => 'test_child_id',
              'call_type' => 'add',
              'route' => '/test/childadd',
              'req_method' => 'post',
              'param' => 
              array (
                0 => 'child_name|1|子名称',
                1 => 'test_id|1|测试ID',
              ),
            ),
            7 => 
            array (
              'method_name' => 'childUpdate',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'childUpdate',
              'comment' => '更新测试子数据',
              'controller_name' => 'test',
              'call_pk' => 'test_child_id',
              'call_type' => 'update',
              'route' => '/test/childupdate',
              'req_method' => 'post',
              'param' => 
              array (
                0 => 'test_child_id|1|',
                1 => 'child_name|0|子名称',
              ),
            ),
            8 => 
            array (
              'method_name' => 'childList',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'childList',
              'comment' => '查询测试子数据列表',
              'controller_name' => 'test',
              'call_pk' => 'test_child_id',
              'call_type' => 'list',
              'route' => '/test/childlist',
              'req_method' => 'get',
              'param' => 
              array (
                0 => 'child_name|0|子名称',
              ),
            ),
            9 => 
            array (
              'method_name' => 'childDelete',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'childDelete',
              'comment' => '删除测试子数据',
              'controller_name' => 'test',
              'call_pk' => 'test_child_id',
              'call_type' => 'delete',
              'route' => '/test/childdelete',
              'req_method' => 'post',
              'param' => 
              array (
                0 => 'test_child_id|1|',
              ),
            ),
            10 => 
            array (
              'method_name' => 'childInfo',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'childInfo',
              'comment' => '获取测试子数据详情',
              'controller_name' => 'test',
              'call_pk' => 'test_child_id',
              'call_type' => 'info',
              'route' => '/test/childinfo',
              'req_method' => 'get',
              'param' => 
              array (
                0 => 'test_child_id|1|',
              ),
            ),
            11 => 
            array (
              'method_name' => 'testCheck',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'testCheck',
              'comment' => '检测测试数据',
              'controller_name' => 'test',
              'call_pk' => 'test_id',
              'call_type' => 'find',
              'route' => '/test/testcheck',
              'req_method' => 'get',
              'param' => 
              array (
                0 => 'test_id|0|',
              ),
            ),
            12 => 
            array (
              'method_name' => 'statusChange',
              'call_namespace' => 'app\\service',
              'call_logic' => 'TestService',
              'call_method' => 'testUpdate',
              'comment' => '启用禁用',
              'controller_name' => 'test',
              'call_pk' => 'test_id',
              'call_type' => 'update',
              'route' => '/test/status',
              'req_method' => 'post',
              'param' => 
              array (
                0 => 'test_id|1|测试ID',
                1 => 'status|1|状态',
              ),
            ),
          ),
        ),
      ),
      'route' => '/**测试数据*/
Route::post(/test/add,\'test/testAdd\');//新增测试数据
Route::post(/test/update,\'test/testUpdate\');//更新测试数据
Route::get(/test/list,\'test/testList\');//查询测试数据列表
Route::post(/test/delete,\'test/testDelete\');//删除测试数据
Route::get(/test/info,\'test/testInfo\');//获取测试数据详情
Route::get(/test/find,\'test/testFind\');//查询测试数据详情
Route::post(/test/childadd,\'test/childAdd\');//新增测试子数据
Route::post(/test/childupdate,\'test/childUpdate\');//更新测试子数据
Route::get(/test/childlist,\'test/childList\');//查询测试子数据列表
Route::post(/test/childdelete,\'test/childDelete\');//删除测试子数据
Route::get(/test/childinfo,\'test/childInfo\');//获取测试子数据详情
Route::get(/test/testcheck,\'test/testCheck\');//检测测试数据
Route::post(/test/status,\'test/statusChange\');//启用禁用

',
      'api' => 
      array (
        0 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '新增测试数据',
          'path' => '/test/add',
          'req' => 'test_name|1|名称,test_desc|0|描述,price|1|价格,status|0|状态&1 启用 0 禁用',
          'http_method' => 'post',
        ),
        1 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '更新测试数据',
          'path' => '/test/update',
          'req' => 'test_id|1|,test_name|0|名称,test_desc|0|描述,price|0|价格,status|0|状态&1 启用 0 禁用',
          'http_method' => 'post',
        ),
        2 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '查询测试数据列表',
          'path' => '/test/list',
          'req' => 'test_name|0|名称,price|0|价格,status|0|状态&1 启用 0 禁用',
          'http_method' => 'get',
        ),
        3 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '删除测试数据',
          'path' => '/test/delete',
          'req' => 'test_id|1|',
          'http_method' => 'post',
        ),
        4 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '获取测试数据详情',
          'path' => '/test/info',
          'req' => 'test_id|1|',
          'http_method' => 'get',
        ),
        5 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '查询测试数据详情',
          'path' => '/test/find',
          'req' => 'test_name|0|名称,price|0|价格,status|0|状态&1 启用 0 禁用',
          'http_method' => 'get',
        ),
        6 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '新增测试子数据',
          'path' => '/test/childadd',
          'req' => 'child_name|1|子名称,test_id|1|测试ID',
          'http_method' => 'post',
        ),
        7 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '更新测试子数据',
          'path' => '/test/childupdate',
          'req' => 'test_child_id|1|,child_name|0|子名称',
          'http_method' => 'post',
        ),
        8 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '查询测试子数据列表',
          'path' => '/test/childlist',
          'req' => 'child_name|0|子名称',
          'http_method' => 'get',
        ),
        9 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '删除测试子数据',
          'path' => '/test/childdelete',
          'req' => 'test_child_id|1|',
          'http_method' => 'post',
        ),
        10 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '获取测试子数据详情',
          'path' => '/test/childinfo',
          'req' => 'test_child_id|1|',
          'http_method' => 'get',
        ),
        11 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 1,
          'title' => '检测测试数据',
          'path' => '/test/testcheck',
          'req' => 'test_id|0|',
          'http_method' => 'get',
        ),
        12 => 
        array (
          'cate_name' => '测试数据',
          'except_token' => 0,
          'title' => '启用禁用',
          'path' => '/test/status',
          'req' => 'test_id|1|测试ID,status|1|状态',
          'http_method' => 'post',
        ),
      ),
    ),
  ),
)