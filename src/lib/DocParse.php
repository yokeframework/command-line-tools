<?php
namespace command\lib;

/**
 * 解析文档注释
 */
class DocParse
{
    /**路由映射 */
    private $route = '';

    /**接口调用方式 */
    private $httpMethod = '';

    /**路由地址 */
    private $path = '';

    /**注释描述 */
    private $comment = '';

    /**返回参数 */
    private $req = [];

    /**api路由分类 */
    private $apiCat = '';
    
    /**
     * 不需要token验证的接口列表
     *
     * @var string
     */
    private $tokenExcept = [];
    
    /**
     * 路由对应的方法
     *
     * @var string
     */
    private $method = '';

    /**
     * 构造函数
     *
     * @param string $method 路由对应类方法
     * @param string $docString 注释字符串
     */
    public function __construct($docString,$method='')
    {
        $this->method = $method;
        $info = $this->parseDoc($docString);

    }

    /**
     * 解析注释
     *
     * @param string $docString 注释字符串
     * @return void
     */
    private function parseDoc($docString)
    {
        $docString = str_replace(["/**","*/"," *"],'',$docString);//过滤 注释标识符

        $list  = explode("@",$docString);//拆分注释内容

        foreach ($list as $key => $value) {
            if($key==0){//拆分后第一个值为注释描述
                $this->comment = trim($value);
                continue;
            }
            $arr = explode('(',$value);//根据商定格式解析每个值
            if(is_callable([$this,'set'.$arr[0]])){//如果解析后为可执行方法，传入参数并执行对应方法
                $paramStr = str_replace(')','',trim($arr[1]??''));
                $method = 'set'.$arr[0];
                $this->$method(explode(",",$paramStr));
            }
        }
    }

    /**
     * 根据注释拼接路由字符串
     * 
     * @param array $param
     */
    public function setRoute($param)
    {
        $this->httpMethod = $param[1]??'get';

        $this->path = trim($param[0],"'");

        $this->route  = "Route::".$this->httpMethod."(".$param[0].",'".$this->method."');//".$this->comment.PHP_EOL;

    }

    /**
     * 设置api请求参数
     *
     * @param array $param
     * @return void
     */
    public function setReq($param)
    {
        $req = [];
        foreach ($param as $key => $value) {
            $req[] = trim($value);
        }
        $this->req = $req;
    }

    /**
     * 设置api分类
     *
     * @param array $param
     * @return void
     */
    public function setApiCat($param)
    {
        $this->apiCat = $param[0];
    }

    /**
     * 设置不校验token的接口列表
     *
     * @param array $param
     * @return void
     */
    public function setTokenExcept($param)
    {
        $this->tokenExcept = $param;
    }

    /**
     * 设置api返回参数
     *
     * @param array $param
     * @return void
     */
    public function setRes($param)
    {
        $this->res = $param;
    }

    /**
     * 设置路由分组
     *
     * @return void
     */
    public function setGroup()
    {
        return $this->group;
    }

    /**
     * 获取路由字符串
     *
     * @return void
     */
    public function getRoute()
    {
        return $this->route;
    }
    
    /**
     * 获取路由地址
     *
     * @return void
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * 获取api 请求json数据
     *
     * @return array
     */
    public function getReq()
    {
        return $this->req;
    }

    /**
     * 获取接口请求方式
     *
     * @return void
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * 获取注释描述
     *
     * @return void
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * 获取api分类
     *
     * @return void
     */
    public function getApiCat()
    {
        return $this->apiCat;
    }

    /**
     * 获取不用校验token接口列表
     *
     * @return array
     */
    public function getTokenExcept()
    {
        return $this->tokenExcept;
    }
}