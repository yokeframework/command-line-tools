<?php
declare (strict_types = 1);

namespace command\logic;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Config;

class ServiceCreate extends Command
{

    protected function configure()
    {
        // 指令配置
        $this->setName('ykservice:create')
            ->addArgument('module', Argument::OPTIONAL, "app module name")
            ->addOption('Services', null, Option::VALUE_REQUIRED, 'op service list')
            ->setDescription('auto create service');        
    }

    protected function execute(Input $input, Output $output)
    {

        //获取项目配置，根据项目配置写入逻辑方法
        $projectFile = $this->app->getBasePath().DIRECTORY_SEPARATOR.'Project.php';
        if(is_file($projectFile)){
            $projectConfig = include_once $projectFile;
            $serviceConfigList = $projectConfig['service']??[];
        }else{
            $serviceConfigList = [];
        }

        if(empty($serviceConfigList)){
            $output->writeln('service config is empty');
            return true;
        }

        if ($input->hasOption('services')) {
            $services = $input->getOption('services');
            $services = explode(',',$services);
        }else{
            $services = [];
        }


        $path = $this->app->getBasePath()."service";

        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        $stub = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'service.stub');

       
        foreach ($serviceConfigList as $serviceName => $config) {

            $namespace = 'app\service';

            $pathName = $path . DIRECTORY_SEPARATOR . $serviceName ."Service.php";

            if(is_file($pathName))continue;//文件已存在跳过

            $method = $this->buildMethod($config['service_model']);

            file_put_contents(
                $pathName,
                str_replace(
                    ['{$namespace}','{$use}','{$comment}','{$serviceName}','{$serviceMethod}'],
                    [$namespace,$method['use_str'],$config['comment'],$serviceName,$method['method_str']],
                    $stub
                )
            );

        }

    	// 指令输出
    	$output->writeln('service');
    }

    /**
     * 构建逻辑处理方法
     *
     * @param array $modelConfig
     * @param string $modelName
     * @return void
     */
    private function buildMethod($serviceModel)
    {
        $use = [];
        $functions = [];
        foreach ($serviceModel as $model=>$config) {
            if(empty($config['functions']))continue;
            $use[]='use '.($config['namespace']??'app\\model').'\\'.$model.';';

            foreach ($config['functions'] as $type => $value) {
                $tplFile = __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'method' . DIRECTORY_SEPARATOR . $type . '.stub';
                if(is_file($tplFile)){
                    $tplstr = file_get_contents($tplFile);
                    $functions[]=str_replace(
                        ['{$comment}','{$methodName}','{$model}','{$pk}','{$allowField}','{$with}','{$order}','{$wconfig}'],
                        [$config['comment'],lcfirst($model),$model,$config['pk'],$value['allowField']??'[]',$value['with']??'""',$value['order']??'""',$value['wconfig']??'[]'],
                        $tplstr
                    );
                }
            }
            
        }

        $res['method_str'] = implode(PHP_EOL.PHP_EOL,$functions);
        $res['use_str'] = implode(PHP_EOL,$use);

        return $res;

    }
}
