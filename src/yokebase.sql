/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50714
 Source Host           : localhost:3306
 Source Schema         : manwantu

 Target Server Type    : MySQL
 Target Server Version : 50714
 File Encoding         : 65001

 Date: 13/12/2019 10:30:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yk_admin
-- ----------------------------
DROP TABLE IF EXISTS `yk_admin`;
CREATE TABLE `yk_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(255) DEFAULT NULL COMMENT '登录名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `salt` varchar(255) DEFAULT NULL COMMENT '密码加盐',
  `admin_name` varchar(255) DEFAULT NULL COMMENT '管理员昵称',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
  `token` varchar(255) DEFAULT NULL COMMENT '登录token',
  `expire` int(11) DEFAULT NULL COMMENT '有效期',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态',
  `type` tinyint(4) DEFAULT '0' COMMENT '是否内置 0 非内置 1 内置',
  `last_login` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for yk_role
-- ----------------------------
DROP TABLE IF EXISTS `yk_role`;
CREATE TABLE `yk_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `menu` longtext COMMENT '菜单权限列表',
  `api` longtext COMMENT '接口权限',
  `status` tinyint(4) DEFAULT '1' COMMENT '1 正常 -1 禁用',
  `type` tinyint(4) DEFAULT '0' COMMENT '是否内置 0 否 1内置',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;;

-- ----------------------------
-- Table structure for yk_user
-- ----------------------------
DROP TABLE IF EXISTS `yk_user`;
CREATE TABLE `yk_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登录名',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `identity_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '身份证',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码盐',
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登录token',
  `expire` int(11) DEFAULT NULL COMMENT '有效期',
  `status` tinyint(4) DEFAULT 1 COMMENT '1 启用 -1 禁用',
  `group_id` int(11) DEFAULT 1 COMMENT '分组ID',
  `rank_id` int(11) DEFAULT 1 COMMENT '等级ID',
  `last_login` datetime(0) DEFAULT NULL COMMENT '最近登录时间',
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `delete_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yk_file
-- ----------------------------
DROP TABLE IF EXISTS `yk_file`;
CREATE TABLE `yk_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL COMMENT '文件地址',
  `hash` varchar(255) DEFAULT NULL COMMENT '文件hash值',
  `use` varchar(255) DEFAULT NULL COMMENT '使用列表',
  `create_time` datetime DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for yk_log
-- ----------------------------
DROP TABLE IF EXISTS `yk_log`;
CREATE TABLE `yk_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '日志标题',
  `api` varchar(255) DEFAULT NULL COMMENT '接口地址',
  `param` text COMMENT '请求参数',
  `ip` varchar(255) DEFAULT NULL COMMENT '请求IP地址',
  `user_agent` text COMMENT '用户终端标识',
  `module` varchar(255) DEFAULT NULL COMMENT '模块',
  `app` varchar(255) DEFAULT NULL COMMENT '应用端',
  `operator_id` int(11) DEFAULT NULL COMMENT '操作人ID',
  `operator_type` varchar(255) DEFAULT NULL COMMENT '操作人类型',
  `operator_name` varchar(255) DEFAULT NULL COMMENT '操作人名称',
  `content` text COMMENT '操作内容',
  `before` text COMMENT '操作前数据',
  `before_data` text COMMENT '操作前原始数据',
  `after` text COMMENT '操作后数据',
  `after_data` text COMMENT '操作后原始数据',
  `type` tinyint(4) DEFAULT '0' COMMENT '类型 1  新增 2 删除 3 修改 4 查询',
  `is_display` tinyint(4) DEFAULT '0' COMMENT '是否可视化展示 0',
  `code` int(11) DEFAULT NULL COMMENT '状态码',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


SET FOREIGN_KEY_CHECKS = 1;
